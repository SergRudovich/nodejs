const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.post('/', createUserValid, (req, res, next) => {
    const user = req.body;
    if (UserService.search({ "email": `${req.body.email}` }) === null &&
        UserService.search({ "phoneNumber": `${req.body.phoneNumber}` }) === null) {
        UserService.create(user);
        res.status(200).json({});
        next();
    } else {
        res.status(400).json({ error: true, message: 'Cannot create user double email or phone' });
    }
}/*, responseMiddleware*/);

router.get('', (req, res, next) => {
    if (true) {
        let data = UserService.getAll();
        res.data = data;
        res.status(200).json({});
        next();
    } else {
        res.status(400).json({ error: true, message: 'Cannot get fighter' });
    }
});

router.get('/', (req, res, next) => {
    try {
        let data = UserService.getOne({ "id": `${req.params.id}` });
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/', updateUserValid, (req, res, next) => {
    try {
        let data = UserService.update(req.params.id, req.body);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/', (req, res, next) => {
    try {
        let user = UserService.delete(req.params.id);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;