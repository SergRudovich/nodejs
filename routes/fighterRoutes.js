const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.post('', createFighterValid, (req, res, next) => {
    const fighter = req.body;
    fighter.health = 100;
    if (FighterService.search({ "name": `${fighter.name}` }) === null) {
        FighterService.create(fighter);
        res.status(200).json({});
        next();
    } else {
        res.status(400).send({ error: true, message: 'Cannot create fighter with the same name' });
    }
});

router.get('', (req, res, next) => {
    if (true) {
        let data = FighterService.getAll();
        res.data = data;
        res.status(200).json({});
        next();
    } else {
        res.status(400).json({ error: true, message: 'Cannot get fighter' });
    }
});

router.get('/', (req, res, next) => {
    try {
        let data = FighterService.getOne({ "id": `${req.params.id}` });
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/', updateFighterValid, (req, res, next) => {
    try {
        let data = FighterService.update(req.params.id, req.body);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/', (req, res, next) => {
    try {
        let user = FighterService.delete(req.params.id);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;