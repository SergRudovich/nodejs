const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    let result = true;
    const fighterPattern = {
        "name": "",
        "power": 0,
        "defense": 1,
    }

    for (let key in fighterPattern) {
        if (!(key in req.body)){
            res.status(400).json({ error: true, message: 'Fighter data invalid all fields reqied' });
            return;
        } 
        
        if (req.body[key] === '') result = false;
    }

    if (!("name" in req.body)) result = false;
    if (!("power" in req.body)) result = false;
    if (!("defense" in req.body)) result = false;

    if (req.body.power < 1 || req.body.power > 100) result = false;
    if (req.body.defense < 1 || req.body.defense > 10) result = false;



    if (result) {
        next();
    } else {
        res.status(400).json({ error: true, message: 'Fighter data invalid' });
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;