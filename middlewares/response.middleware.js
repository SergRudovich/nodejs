const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.err) {
        // console.log(res.err);
        res.status(401).json({ error: true, message: 'Not authorized' });
    }else{
        // res.status(200).send();
        // res.render("/");
        res.status(200).json({});
       next();
    }
}

exports.responseMiddleware = responseMiddleware;