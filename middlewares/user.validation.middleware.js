const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation

  let result = true;
  const phonePattern = /^\+380\d{9}$/;
  const emailPattern = /^[a-zA-Z0-9_.+-]+@gmail.com$/i;
  const userPattern = {
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
    password: ''
  }

  for (let key in userPattern) {
    if (!(key in req.body)){
      res.status(400).json({ error: true, message: 'User data invalid all fields reqied' });
      return;
    }
      
    if (req.body[key] === '') result = false;
  }

  if (!("email" in req.body)) result = false;
  if (!("firstName" in req.body)) result = false;
  if (!("lastName" in req.body)) result = false;
  if (!("phoneNumber" in req.body)) result = false;
  if (!("password" in req.body)) result = false;

  if (req.body.password.length < 3) result = false;

  if (!phonePattern.test(req.body.phoneNumber)) result = false;

  if (!emailPattern.test(req.body.email)) result = false;

  if (result) {
    next();
  } else {
    res.status(400).json({ error: true, message: 'User data invalid' });
  }
}

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update

  next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;