const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(fighter){
        const item = FighterRepository.create(fighter);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll(){
        const item = FighterRepository.getAll();
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = FighterRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }

    getOne(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

}

module.exports = new FighterService();