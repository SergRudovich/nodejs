const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    create(user){
        const item = UserRepository.create(user);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = UserRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const item = UserRepository.getAll();
        if(!item) {
            return null;
        }
        return item;
    }

    getOne(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();